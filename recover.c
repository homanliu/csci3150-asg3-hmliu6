#include "disk_info.h"

/*
  You can use function traverse_directory_find_deleted() and check_occupied() to finish this function
*/
int locate_deleted_file(struct DiskInfo *disk_info, 
                        const char *recover_target, 
                        struct msdos_dir_entry *target_dir_entry, 
                        int cleanse) {
    char *dirc, *basec;
    dirc = strdup(recover_target);
    basec = strdup(recover_target);
    char *recover_target_dirname = dirname(dirc);
    if(recover_target_dirname == NULL)
        die();
    char *recover_target_basename = basename(basec);
    if(recover_target_basename == NULL)
        die();
    
    int ret = traverse_directory_find_deleted(disk_info, recover_target_dirname, cleanse, recover_target_basename, target_dir_entry);
    if(ret){
        if(!check_occupied(disk_info, target_dir_entry)){
            if(!cleanse)
                printf("%s: recovered\n", recover_target_basename);
            free(dirc);
            free(basec);
            return 1;
        }
        else{
            if(!cleanse)
                printf("%s: error - fail to recover\n", recover_target_basename);
            else
                printf("%s: error - fail to cleanse\n", recover_target_basename);
            free(dirc);
            free(basec);
            exit(0);
        }
    }
    free(dirc);
    free(basec);
    return 0;
}

/* 
You can reuse most of the codes in the copy_deleted_file() function.
*/
int cleanse_deleted_file(struct DiskInfo *disk_info, struct msdos_dir_entry dir_entry, const char *cleanse_target) {
    char *dirc, *basec;
    dirc = strdup(cleanse_target);
    basec = strdup(cleanse_target);
    char *cleanse_target_dirname = dirname(dirc);
    if(cleanse_target_dirname == NULL)
        die();
    char *cleanse_target_basename = basename(basec);
    if(cleanse_target_basename == NULL)
        die();

    uint32_t dir_first_cluster = get_dir_first_cluster(&dir_entry);
    if(dir_first_cluster != 0) {
        uint32_t next_cluster_number = dir_first_cluster;
        uint32_t current_cluster_number;
        uint32_t file_size = dir_entry.size;
        char *buffer = malloc(disk_info->bpc);
        if(file_size > disk_info->bpc) {
            printf("More than one cluster!!!\n");
            exit(1);
        }

        while(next_cluster_number < FAT_EOF && next_cluster_number != FAT_DAMAGED && next_cluster_number != FAT_UNALLOCATED) {
            current_cluster_number = next_cluster_number;
            off_t cluster_offset = get_cluster_offset(disk_info->root_entry_offset, current_cluster_number, disk_info->bpc);
            fseek(disk_info->fp, cluster_offset, SEEK_SET);
            fwrite(buffer, 1, file_size, disk_info->fp);
            next_cluster_number = next_cluster(disk_info->fp, disk_info->fat_offset,current_cluster_number);
        }
        free(buffer);
        printf("%s: cleansed\n", cleanse_target_basename);
    }
    else{
        printf("%s: error - fail to cleanse\n", cleanse_target_basename);
    }
    return 1;
}
